// @file: promptsBin.js
import axios from 'axios';
// Action Types
import {
  CLEAR_CURRENT_PROMPT,
  GET_PROMPT,
  GET_PROMPTS,
  PROMPT_LOADING,
  CLEAR_ERRORS
} from '../actionTypes';

// Actions
// no route
export const setPromptLoading = () => {
  return { PROMPT_LOADING };
};
// Clear errors
export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};
/*
 *********** GET routes ***********
 */
// export const getPrompts = numberOfPrompts => {
//   if (numberOfPrompts === 0) getAllPrompts();
//   else {
//     getLimitedPrompts();
//   }
// };
// Get all prompts
export const getPrompts = () => dispatch => {
  axios
    // get all prompts
    .get('/api/prompts/')
    .then(res => {
      dispatch({
        type: GET_PROMPTS,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_PROMPTS,
        payload: null
      });
    });
};
// Gets   <numberOfPrompts> latest prompts
export const getLimitedPrompts = numberOfPrompts => dispatch => {
  const data = { limitTo: parseInt(numberOfPrompts) };
  axios
    .post('/api/prompts/', data)
    .then(res => {
      dispatch({
        type: GET_PROMPTS,
        payload: res.data
      });
    })
    .catch(() => {
      dispatch({
        type: GET_PROMPTS,
        payload: null
      });
    });
};
// Get prompt by id
export const getPrompt = id => dispatch => {
  axios
    //
    .get(`/api/prompt/${id}`)
    .then(res => {
      dispatch({
        type: GET_PROMPT,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_PROMPT,
        payload: null
      });
    });
};
//Reducer
export const initialState = {
  prompt: null,
  prompts: null,
  loading: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CLEAR_CURRENT_PROMPT:
      return {
        ...state,
        prompt: null
      };
    case GET_PROMPT: // changes state.post to that prompt.payload
      return {
        ...state,
        prompt: action.payload,
        loading: false
      };
    case GET_PROMPTS:
      return {
        ...state,
        prompts: action.payload,
        loading: false
      };
    case PROMPT_LOADING: // state before fetching prompt; prompt.payload reset loading to false
      return {
        ...state,
        loading: true
      };
    default:
      return state;
  }
};
