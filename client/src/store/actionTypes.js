export const CLEAR_ERRORS = 'CLEAR_ERRORS';
export const GET_ERRORS = 'GET_ERRORS';

// prompt action types
export const CLEAR_CURRENT_PROMPT = 'CLEAR_CURRENT_PROMPT';
export const GET_PROMPT = 'GET_PROMPT';
export const GET_PROMPTS = 'GET_PROMPTS';
export const PROMPT_LOADING = 'PROMPT_LOADING';
export const PROMPT_NOT_FOUND = 'PROMPT_NOT_FOUND';

// post action types
export const ADD_POST = 'ADD_POST';
export const CLEAR_CURRENT_POST = 'CLEAR_CURRENT_POST';
export const EDIT_POST = 'EDIT_POST';
export const DELETE_POST = 'DELETE_POST';
export const GET_POST = 'GET_POST';
export const GET_POSTS = 'GET_POSTS';
export const POST_LOADING = 'POST_LOADING';
export const POST_NOT_FOUND = 'POST_NOT_FOUND';

// profile action types
export const ADD_EXPERIENCE = 'ADD_EXPERIENCE';
export const CLEAR_CURRENT_PROFILE = 'CLEAR_CURRENT_PROFILE';
export const GET_PROFILE = 'GET_PROFILE';
export const GET_PROFILES = 'GET_PROFILES';
export const PROFILE_LOADING = 'PROFILE_LOADING';
export const PROFILE_NOT_FOUND = 'PROFILE_NOT_FOUND';

// user action types
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
