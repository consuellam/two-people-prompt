import { combineReducers } from 'redux';
import promptReducer from './bins/promptsBin';

export default combineReducers({
  prompt: promptReducer
});
