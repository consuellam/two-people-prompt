import React from 'react';
// Redux
import { Provider } from 'react-redux';
import store from './store/store';
// react router
import { BrowserRouter, Route } from 'react-router-dom';

/************ Routes ************/
import Home from './components/routes/Home';
import About from './components/routes/About';

/************ components ************/
import Layout from './components/layouts/Layout';
import Prompts from './components/prompts/Prompts';

// Checks for Current user token

import './App.css';

const App = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Layout className="App">
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/prompts" component={Prompts} />
        </Layout>
      </BrowserRouter>
    </Provider>
  );
};

export default App;
