import React from 'react';
import Prompts from '../prompts/Prompts';
import { Column, Row } from 'simple-flexbox';
/* @file:  Home.js
// @desc: /about  page
*/
const Home = () => {
  return (
    <Column>
      <Row>
        <Prompts numberOfPrompts="0" />
      </Row>
    </Column>
  );
};

export default Home;
