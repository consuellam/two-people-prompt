import React from 'react';

/* @file:  About.js
// @desc: /about  page
*/
const About = () => {
  return (
    <div>
      <h2>About</h2>
      <p>A News app for me to learn, build, and deploy a MERN Stack</p>.
      <p>It is based on this tutorial</p>
    </div>
  );
};

export default About;
