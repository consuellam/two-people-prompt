import React from 'react';

/* @file:  Footer.js
// @desc: container for footer components
*/
const Footer = () => {
  return (
    <div>
      <p>
        Two People Prompt it, MIT license so you can share it any way you
        like
      </p>
    </div>
  );
};

export default Footer;
