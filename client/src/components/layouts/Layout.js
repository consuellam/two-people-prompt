import React from 'react';
//import PropTypes from 'prop-types'
import Header from './Header';
import Footer from './Footer';

/* @file:  Layout.js
// @desc: main layout for the site
*/
const Layout = props => {
  return (
    <div>
      <Header title="Two People Prompt" />
      <div>{props.children}</div>
      <Footer />
    </div>
  );
};

Layout.proptypes = {};

export default Layout;
