import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

/* @file:  Header.js
// @desc: container for header components
*/
const Header = ({ title }) => {
  const navlinks = [
    // [to,text] <NavLink to={to}>{text}</NavLink>
    { to: '/', text: 'home' },
    { to: '/about', text: 'about' },
    { to: '/prompts', text: 'prompts' }
  ];
  const navlinksList = navlinks.map((link, index) => {
    return (
      <li key={index}>
        <NavLink to={link.to}>{link.text}</NavLink>{' '}
      </li>
    );
  });
  return (
    <div>
      <h1>{title}</h1>

      <nav>
        <ul>{navlinksList}</ul>
      </nav>
    </div>
  );
};

Header.proptypes = {
  title: PropTypes.string
};

export default Header;
