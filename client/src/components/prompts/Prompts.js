import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getPrompts, getLimitedPrompts } from '../../store/bins/promptsBin';
import PromptListItem from './PromptListItem';
import { Column, Row } from 'simple-flexbox';
import { Header, Icon } from 'semantic-ui-react';

/* @file  Prompts.js
// @desc  container for prompts list and display
*/
class Prompts extends Component {
  componentDidMount = () => {
    // How many prompts to return 0 returns all of the prompts
    const numberOfPrompts = parseInt(this.props.numberOfPrompts);
    !numberOfPrompts || numberOfPrompts === 0 || isNaN(numberOfPrompts)
      ? this.props.getPrompts()
      : this.props.getLimitedPrompts(numberOfPrompts);
  };

  render() {
    const { prompts, loading } = this.props.prompt;
    let promptList;
    if (prompts === null || loading) {
      promptList = <Icon loading name="spinner" />;
    } else {
      if (prompts.length > 0) {
        promptList = prompts.map(prompt => (
          <PromptListItem key={prompt._id} prompt={prompt} />
        ));
        //promptList = prompts.length;
      } else {
        promptList = <Header as="h4">No Prompts Available</Header>;
      }
    }
    return (
      <Column>
        <Row>
          <Column>{promptList}</Column>
        </Row>
      </Column>
    );
  }
}

Prompts.propTypes = {
  getPrompts: PropTypes.func.isRequired,
  prompt: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  prompt: state.prompt
});

const mapDispatchToProps = {
  getPrompts,
  getLimitedPrompts
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Prompts);
