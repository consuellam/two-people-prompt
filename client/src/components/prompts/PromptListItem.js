import React from 'react';
import PropTypes from 'prop-types';
import { Button, Card, Icon, Label } from 'semantic-ui-react';
import Moment from 'react-moment';

/* @file:  PromptListItem.js
// @desc: Shows a Prompt in list
*/
const PromptListItem = props => {
  const { setting, area, person1, person2, createdAt } = props.prompt;
  let promptString = `Sometime between ${setting.timeOfDay} during the ${
    setting.season
  },
  two people are at the doorway of a ${area.buildingType} building located
  in a ${area.regionFinancialStatus} ${area.regionType} area.

  One is a ${person1.financialStatus} ${person1.gender} around ${person1.age}.
  The other is a ${person2.financialStatus} ${person2.gender} around ${
    person2.age
  }.`;
  const extra = (
    <Button as="div" labelPosition="right">
      <Button basic color="blue">
        <Icon name="write" />
        Stories Published
      </Button>
      <Label as="a" basic color="blue" pointing="left">
        {/*   todo: add count */}0
      </Label>
    </Button>
  );
  // const promptString = 'prompt goes here';
  let created_At = <Moment format="MMMM DD, YYYY" date={createdAt} />;
  return (
    <Card fluid meta={created_At} description={promptString} extra={extra} />
  );
};

PromptListItem.proptypes = {
  prompt: PropTypes.object.isRequired
};

export default PromptListItem;
