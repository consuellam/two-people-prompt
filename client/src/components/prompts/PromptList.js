import React from 'react';
import PropTypes from 'prop-types';
import PromptListItem from './PromptListItem';

/* @file:  PromptList.js
// @desc: List of Posts
*/
const PromptList = props => {
  const { prompts } = props;
  return prompts.map(prompt => (
    <PromptListItem key={prompt._id} prompt={prompt} />
  ));
};

PromptList.proptypes = {
  prompts: PropTypes.array.isRequired
};

export default PromptList;
