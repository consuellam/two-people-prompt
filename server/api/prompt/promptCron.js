const cron = require('node-cron');
const Prompt = require('./promptModel');
const createPrompt = require('./createPrompt');

const savePrompt = cron.schedule(
  '* * * * Sunday',
  function() {
    // saves a new Prompt to the database every Sunday at midnight
    const newPrompt = new Prompt(createPrompt());
    newPrompt
      .save()
      .then(console.log(`prompt saved`))
      .catch(err => console.log(err));
  },
  true,
  'America/New_York'
);

module.exports = savePrompt;
