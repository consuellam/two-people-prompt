const mongoose = require('mongoose');

const promptSchema = new mongoose.Schema(
  {
    storiesPublished: {
      type:Number,
      default: 0
    },
    setting: {
      timeOfDay: {
        type: String,
        required: [true, 'Time of Day is required']
      },
      season: {
        type: String,
        required: [true, 'Season is required'],
        enum: ['Spring', 'Summer', 'Fall', 'Winter']
      }
    },
    area: {
      buildingType: {
        type: String,
        required: [true, 'Building Type is required']
      },
      regionFinancialStatus: {
        type: String,
        required: [true, 'Region Financial Status is required']
      },
      regionType: {
        type: String,
        required: [true, 'Region Type is required']
      }
    },
    person1: {
      gender: {
        type: String,
        required: [true, `Person 1's Gender is required`]
      },
      age: {
        type: String,
        required: [true, `Person 1's Age is required'`]
      },
      financialStatus: {
        type: String,
        required: [true, `Person 1's Financial Status is required`]
      }
    },
    person2: {
      gender: {
        type: String,
        required: [true, `Person 2's Gender is required`]
      },
      age: {
        type: String,
        required: [true, `Person 2's Age is required'`]
      },
      financialStatus: {
        type: String,
        required: [true, `Person 2's Financial Status is required`]
      }
    }
  },
  { timestamps: true }
);

module.exports = Prompt = mongoose.model('Prompt', promptSchema);
