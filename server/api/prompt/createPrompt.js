const getRandomChoice = iter =>
  // @desc: returns a randomly chosen element for iter
  // @params: arr - an iterable element
  iter[Math.floor(Math.random() * iter.length)];

const settings = () => {
  return {
    timeOfDay: getRandomChoice([
      '12 am - 4 am',
      '4 am - 8 am',
      '8 am - 12 pm',
      '12 pm - 4 pm',
      '4 pm - 8 pm',
      '8 pm - 12 pm'
    ]),
    season: getRandomChoice(['Spring', 'Summer', 'Fall', 'Winter'])
  };
};
const setArea = () => {
  return {
    buildingType: getRandomChoice([
      'business',
      'educational',
      'entertainment',
      'industrial',
      'government',
      'legal',
      'medical',
      'military',
      'religious',
      'residential',
      'retail',
      'other',
      'sports'
    ]),
    regionFinancialStatus: getRandomChoice([
      'very rich',
      'rich',
      'upper-middle class',
      'middle class',
      'lower-middle class',
      'poor',
      'very poor'
    ]),
    regionType: getRandomChoice([
      'small rural',
      'small suburban',
      'small urban',
      'mid-size rural',
      'mid-size suburban',
      'mid-size urban',
      'large rural',
      'large suburban',
      'large urban'
    ])
  };
};

const person = () => {
  return {
    gender: getRandomChoice(['male', 'female']),
    age: getRandomChoice([
      '0 - 4',
      '4 - 10',
      '10 - 15',
      '15 - 20',
      '20 - 30',
      '30 - 45',
      '45 - 60',
      '60 - 75',
      '75+'
    ]),
    financialStatus: getRandomChoice([
      'very rich',
      'rich',
      'upper middle class',
      'middle class',
      'lower middle class',
      'poor',
      'very poor'
    ])
  };
};
module.exports = function createPrompt() {
  // let setting = settings();
  // Randomize prompt strings
  const setting = settings();
  const area = setArea();
  const person1 = person();
  const person2 = person();
  return { setting, area, person1, person2 };
  // console.log(setting);
  // return { setting };
};
