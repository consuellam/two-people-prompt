/* @file:  promptRoutes.js
// @desc: /api/prompt   
*/
const express = require('express');
const router = express.Router();

//Models
const Prompt = require('./promptModel');

// ------------------- Route test --------------------------------------
// @route   GET api/prompt/test
// @desc    Tests prompt route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: '/api/prompt Test works' }));
// ------------------- --------------------------------------

// @route    /api/prompt/
// @desc     Gets all of the prompts
// @access   Public
router.get('/', (req, res) => {
  Prompt.find()
    .sort({ createdAt: -1 })
    .limit()
    .then(prompts => res.json(prompts))
    .catch(err => res.json(err));
});
// @route    /api/prompt/
// @desc      all of the prompts
// @access   Public
router.post('/', (req, res) => {
  if (req.body.limitTo && Number.isInteger(req.body.limitTo) === false) {
    return res.json(400, 'limitTo must be an integer');
  }
  const limitTo = req.body.limitTo || 3;
  Prompt.find()
    .sort({ createdAt: -1 })
    .limit(limitTo)
    .then(prompts => res.json(prompts))
    .catch(err => res.json(err));
});

module.exports = router;
