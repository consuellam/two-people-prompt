const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const app = express();
const port = process.env.PORT || 5000;
require('dotenv').config();
const savePrompt = require('./api/prompt/promptCron');
const inProduction = process.env.NODE_ENV === 'production';
/************ Route Imports ************/
const prompt = require('./api/prompt/promptRoutes');
/************ Middleware ************/
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//==
// MongoDB
const dbUrl = process.env.DB_URL || process.env.DEV_DB_URL;
mongoose.Promise = global.Promise;
mongoose
  .connect(
    dbUrl,
    { useNewUrlParser: true }
  )
  .then(
    () => {
      console.log('Connected to database');
    },
    err => {
      console.log(`Can not connect to database
err`);
    }
  );
savePrompt.start();
//
// Serve any static files
if (inProduction) {
  app.use(express.static(path.join(__dirname, '../client/build')));
}
// *** API Request Routes
//
app.use('/api/prompts', prompt);
app.get('/', (req, res) => {
  res.send({ message: 'Two People Prompt' });
});
app.get('/api', (req, res) => {
  res.send({ message: 'Two People Prompt API' });
});

// @route    *
// @desc     All other unhandle request returns the React client index.html
// @access   Public
if (inProduction) {
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/build/index.html'));
  });
} else {
  app.use(function(req, res) {
    res.status(404).send("Sorry can't find that!");
  });
}
app.listen(port, () => console.log(`Listening on port ${port}`));
